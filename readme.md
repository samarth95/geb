# Example Geb and Gradle Project

[![Build Status][build_status]](https://circleci.com/gh/geb/geb-example-gradle/tree/master)

## Description

This is an example of incorporating Geb into a Gradle build. It shows the use of Spock and JUnit 4 tests.

The build is setup to work with Firefox and Chrome. Have a look at the `build.gradle` and the `src/test/resources/GebConfig.groovy` files.

## Usage

The following commands will launch the tests with the individual browsers:

    ./gradlew clean chromeTest
    ./gradlew clean firefoxTest

To run with all, you can run:

    ./gradlew clean test

To run the test cases in headless mode

    ./gradlew chromeHeadless

To run a particular test case

    ./gradlew clean {TaskName} --tests=com.bdsi.webtest.{TestName}

    example -
            ./gradlew clean chromeTest --tests=com.bdsi.webtest.Home

To run specific function of specific test case

    ./gradlew clean chromeTest --tests=com.bdsi.webtest.{TestCaseName}."{FunctionName}"

     example-

    ./gradlew clean chromeTest --tests=com.bdsi.webtest.Home."Footer links Present"

To run test cases on desired environment

    ./gradlew clean -DBASE_URL=<env name> clean chromeTest

    example -
            ./gradlew clean -DBASE_URL=stg1 chromeTest

Replace `./gradlew` with `gradlew.bat` in the above examples if you're on Windows.

## Questions and issues

Please ask questions on [Geb user mailing list][mailing_list] and raise issues in [Geb issue tracker][issue_tracker].


[build_status]: https://circleci.com/gh/geb/geb-example-gradle/tree/master.svg?style=shield&circle-token=38eb8de9af8f889922b91624a7943c474c0c3617 "Build Status"
[mailing_list]: https://groups.google.com/forum/#!forum/geb-user
[issue_tracker]: https://github.com/geb/issues/issues
