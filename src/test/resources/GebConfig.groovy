import com.aoe.gebspockreports.GebReportingListener
import geb.driver.CachingDriverFactory
import io.github.bonigarcia.wdm.WebDriverManager
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxDriver

reportingListener = new GebReportingListener()
/** driver will throw an error if driver doesn't find the element in 20 seconds*/
waiting {
	timeout = 60
}


CachingDriverFactory.clearCacheAndQuitDriver()


environments {
	// run via “./gradlew chromeTest”
	chrome {
		WebDriverManager.chromedriver().setup()
		driver = { new ChromeDriver() }
	}
	// run via “./gradlew chromeHeadlessTest”
	chromeHeadless {
		driver = {
			WebDriverManager.chromedriver().setup()
			ChromeOptions o = new ChromeOptions()
			o.addArguments('headless')
			new ChromeDriver(o)
		}
	}
	// run via “./gradlew firefoxTest”
	firefox {
		WebDriverManager.firefoxdriver().setup()
		driver = { new FirefoxDriver() }
	}
}

/**
 The baseUrl _has_ to have a trailing slash or the tests will fail to use it
 */
//baseUrl = "https://bea-stg1-store.sparkred.com/"

baseUrl = com.bdsi.util.BDSISpockUtils.getConfigProperty('baseUrl')
userName = com.bdsi.util.BDSISpockUtils.getConfigProperty('userName')
password = com.bdsi.util.BDSISpockUtils.getConfigProperty('password')

