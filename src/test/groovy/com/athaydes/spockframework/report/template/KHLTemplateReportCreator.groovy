package com.athaydes.spockframework.report.template

import com.athaydes.spockframework.report.internal.SpecData
import com.athaydes.spockframework.report.util.Utils
import groovy.util.logging.Slf4j

/**
 * IReportCreator which uses a user-provided template to generate spock-reports.
 */
@Slf4j
class KHLTemplateReportCreator extends TemplateReportCreator {
	static final reportsAggregator = new TemplateReportAggregator()
	def allFeatues = [] as List<HashMap>
	
    String emailSummaryFileName
	String emailSummaryTemplateFile
    boolean enabled = true

	@Override
	void done() {
		super.done()
		def reportsDir = Utils.createDir( outputDir )
		reportsAggregator.writeOut(
				new File(reportsDir, emailSummaryFileName),
				emailSummaryTemplateFile)
	}

}
