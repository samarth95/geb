/*
package com.bdsi.webtest

import com.bdsi.webtest.pages.CartPage
import com.bdsi.webtest.pages.HomePage
import geb.spock.GebReportingSpec
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import spock.lang.Title
import spock.lang.Unroll

@Title(value = "Quantity Split")
class CartFunctionality extends GebReportingSpec{

    */
/*
    This test cases will check all the cart related functionalities
    1. Select different shipping methods.
    2.Line split functionality.
    3.Create quotes
     *//*


    */
/* Utility methods     *//*

    def "setup"() {
        driver.manage().window().maximize()
    }

    static ccontent={
        qtysplitbox(wait:true){$(".font-color qty-for-split number-only")}
    }

    void scrollUp(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${-distance});");
        sleep(100)
    }
    void scrollDown(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${distance});");
        sleep(100)
    }

    boolean checkCart(){
        if(viewCartMini.displayed) {
        return true;
        }
        else{
            return false;
        }
    }

    void emptyCart(){
        if(CartPage.updateAll.isDisplayed()){
            CartPage.updateAll.click()
        }
        CartPage.emptyAll.click()
        searchForPart.click()
    }

    */
/* Test Cases start here *//*


    def "Quantity split functionality"(){
        given: "User opens E-comm Website"
        to HomePage
        when: "User log's in and verify if there are part present in cart"
        Login()
        if(checkCart()){
            then:"Some parts are present in the cart so User navigates to cart"
            viewCartMini.click()
            at CartPage
            if(updateAll.displayed){
                updateAll.click()
            }
            emptyAll.click()
            searchForPart.click()
        }
        then:"User navigates back to home page"
            to HomePage
        search.SearchPart("An3-5",1000)
        addPart.click()
        then:"User verifies that part is added to the cart"
        sleep(3000)
        if(viewCartMini.displayed)
        then:"User navigate to the cart"
        viewCartMini.click()
        then:"USer is on cart page"
        at CartPage
        then:"User clicks on EDIT link to update the quantitiy"
        qtyUpdate.click()
        driver.findElement(By.linkText("SPLIT QUANTITY")).click()
        then:"User enters amount of qty update"
        qtysplitbox.value(100)
        then:"User clicks on UPDATE"
        splitQuantityUpdate.click()
        splitQuantityOk.click()
        then:"Qty is splited"
        qtysplit.displayed
        then:"User logs out"
        Logout()
        }
}
*/
