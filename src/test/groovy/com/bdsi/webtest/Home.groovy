package com.bdsi.webtest

import geb.spock.GebReportingSpec
import org.openqa.selenium.By
import spock.lang.Title
import com.bdsi.webtest.pages.HomePage


@Title(value = "Home Page")
class Home extends GebReportingSpec{
static boolean result=true
    def "setup"(){
        driver.manage().window().maximize()
    }

    def "Home Page elements are present"(){
        given:"User opens E-comm website"
        to HomePage
        when:"User Verifies Homepage Elements"
        then:"Boeing Logo is present"
        logo.displayed
        then:"Login|Register Link Present"
        login.displayed
        then:"Products Link is Present"
        driver.findElement(By.linkText("Products & Services")).displayed
        then:"Solutions Link is Present"
        driver.findElement(By.linkText("Solutions")).displayed
        then:"Quality Link is Present"
        driver.findElement(By.linkText("Quality")).displayed
        then:"Resources Link is Present"
        driver.findElement(By.linkText("Resources")).displayed
        then:"News & Events Link is Present"
        driver.findElement(By.linkText("News & Events")).displayed
        then:"About Us Link is Present"
        driver.findElement(By.linkText("About Us")).displayed
        then:"Careers Link is Present"
        driver.findElement(By.linkText("Careers")).displayed
    }

    def "Footer links Present"(){
        given:"User opens E-comm website"
        to HomePage
        when:"User Verifies Footer Elements"
        then:"Fasteners link is Present"
        driver.findElement(By.linkText("Fasteners")).displayed
        then:"Bearings link is Present"
        driver.findElement(By.linkText("Bearings")).displayed
        then:"Seals link is Present"
        driver.findElement(By.linkText("Seals")).displayed
        then:"Honeywell™ Proprietary Parts link is Present"
        driver.findElement(By.linkText("Honeywell™ Proprietary Parts")).displayed
        then:"Chemicals & Specialty Materials link is Present"
        driver.findElement(By.linkText("Chemicals & Specialty Materials")).displayed
        then:"Engineered Products link is Present"
        driver.findElement(By.linkText("Engineered Products")).displayed
        then:"Electrical link is Present"
        driver.findElement(By.linkText("Electrical")).displayed
        then:"Tooling link is Present"
        driver.findElement(By.linkText("Tooling")).displayed
        then:"Lighting link is Present"
        driver.findElement(By.linkText("Lighting")).displayed
        then:"Integrated Supply Chain Solutions link is Present"
        driver.findElement(By.linkText("Integrated Supply Chain Solutions")).displayed
        then:"Quality Policy link is Present"
        driver.findElement(By.linkText("Quality Policy")).displayed
        then:"Quality System Approvals link is Present"
        driver.findElement(By.linkText("Quality System Approvals")).displayed
        then:"Case Studies link is Present"
        driver.findElement(By.linkText("Case Studies")).displayed
        then:"Inventory Stock Check link is Present"
        driver.findElement(By.linkText("Inventory Stock Check")).displayed
        then:"Application for Credit Form link is Present"
        driver.findElement(By.linkText("Application for Credit Form")).displayed
        then:"Contact link is Present"
        driver.findElement(By.linkText("Contact")).displayed
        then:"Supplier link is Present"
        driver.findElement(By.linkText("Supplier")).displayed
        then:"Supplier Communications link is Present"
        driver.findElement(By.linkText("Supplier Communications")).displayed
        then:"Supplier Portal link is Present"
        driver.findElement(By.linkText("Supplier Portal")).displayed
        then:"Defense Supplier Resources link is Present"
        driver.findElement(By.linkText("Defense Supplier Resources")).displayed
        then:"News link is Present"
        driver.findElement(By.linkText("News")).displayed
        then:"Insights link is Present"
        driver.findElement(By.linkText("Insights")).displayed
        then:"Events link is Present"
        driver.findElement(By.linkText("Events")).displayed
        then:"Strategic Acquisitions link is Present"
        driver.findElement(By.linkText("Strategic Acquisitions")).displayed
        then:"Industry Awards & Recognition link is Present"
        driver.findElement(By.linkText("Industry Awards & Recognition")).displayed
        then:"Leadership Team link is Present"
        driver.findElement(By.linkText("Leadership Team")).displayed
        then:"Locations & Contact link is Present"
        driver.findElement(By.linkText("Locations & Contact")).displayed
        then:"Frequently Asked Questions link is Present"
        driver.findElement(By.linkText("Frequently Asked Questions")).displayed
        then:"Join our team link is Present"
        driver.findElement(By.linkText("Join our team")).displayed
        then:"Privacy Policy link is Present"
        driver.findElement(By.linkText("Privacy Policy")).displayed
        then:"EU Data Privacy Notice link is Present"
        driver.findElement(By.linkText("EU Data Privacy Notice")).displayed
        then:"Cookie Settings link is Present"
        driver.findElement(By.linkText("Cookie Settings")).displayed
        then:"Terms of Use link is Present"
        driver.findElement(By.linkText("Terms of Use")).displayed
        then:"Conditions of Sale link is Present"
        driver.findElement(By.linkText("Conditions of Sale")).displayed
        then:"BDSI RMA policy link is Present"
        driver.findElement(By.linkText("BDSI RMA policy")).displayed

    }


    def "Website_opened"() {
        when: "User open's E-comm Website"
        to HomePage
        then: "Verify that E-comm Website opened Properly"
        pageTitle.displayed
    }
/*
    @Unroll
    def "Footer Link #link is Present"(){
        given:"User opens E-comm website"
        to HomePage
        when:"User verifies that #text is present"
        if(driver.findElement(By.linkText(link)).isDisplayed())result=true
        else result=false
        then:"checking"
        where:"#link is present"
        link                            |text
        "First Time User Guide"         |"First Time User Guide"
        "Frequently Asked Questions"    |"Frequently Asked Questions"
        "Register"                      |"Register"
        "Feedback"                      |"Feedback"
        "Contact Support"               |"Contact Support"
        "Privacy Policy"                |"Privacy Policy"
        "EU Data Privacy Notice"        |"EU Data Privacy Notice"
        "Cookie Settings"               |"Cookie Settings"
        "Terms of Use"                  |"Terms of Use"
        "Conditions of Sale"            |"Conditions of Sale"
        "BDSI RMA policy"               |"BDSI RMA policy"

    }*/

}

