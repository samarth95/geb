package com.bdsi.webtest.pages

import org.openqa.selenium.By
import geb.Page

class HomePageAnonymous extends Page {

    static at = {
        heading.contains("Distribution Services")
    }


    static content = {
        heading { $(By.xpath("//span[@id='']")).text() }

    }
}
