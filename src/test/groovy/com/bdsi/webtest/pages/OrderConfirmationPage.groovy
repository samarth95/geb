package com.bdsi.webtest.pages

import geb.Page
import org.openqa.selenium.By

class OrderConfirmationPage extends Page {
    static at={
        title.contains("Order Confirmation - Boeing Distribution")
    }
    static content={
        updateAll(wait: 10, required : false){$("button#updateLineMinimum")}
        //emptyAll(wait:true){ By.xpath("//span[contains(text(),'Empty Cart')]")}
        emptyAll(wait:true){$(By.xpath("//span[contains(text(),'Empty Cart')]"))}
        //searchForPart(wait:true){$("a.btn pmd-btn-flat pmd-ripple-effect btn-default search-btn")}
        searchForPart(wait:true){$(By.xpath("//a[contains(text(),'Search For Parts')]"))}
        qtyUpdate(wait: true){$(By.xpath("/html[1]/body[1]/div[1]/section[1]/div[1]/div[1]/div[4]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[9]/button[1]"))}
        //qtyUpdate(wait :true){$("button", text:"EDIT")}
        //qtyUpdate(wait :true){$("td", class:contains("text"), text:"EDIT").click()}
        //splitQuantity(wait: true){$(By.xpath("//span[contains(text(),'SPLIT QUANTITY')]"))}
        splitQuantity(wait: true){$(By.xpath("i.split-text"))}
        splitQuantityBox(wait: true){By.xpath("//tbody/tr[1]/td[1]/input[1]")}
        splitQuantityUpdate(wait: true){By.xpath("//div[contains(text(),'UPDATE')]")}
        splitQuantityOk(wait: true){By.xpath("//button[@id='splitLine']")}
        qtysplit(wait: true){By.linkText("QTY SPLIT")}
        logoutIcon(wait: true) { $("img.user-icon") }
        logoutLink(wait: true) { $(By.xpath("//a[@id='logOutButton']")) }

    }

}
