package com.bdsi.webtest.pages

import geb.Page
import org.openqa.selenium.By

class SavedQuotesPage extends Page{
    static at = {
        sleep(3000)
        title.contains("QUOTES - Boeing Distribution")
    }

    static content={
        createdSavedQuoteName(wait:true){$("div.table-container div span.table-name")[0]}
    }
}
