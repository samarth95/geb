package com.bdsi.webtest.pages

import geb.Page

class ProfilePage extends Page {
    static at={
        title.contains("My Account - Boeing Distribution")
    }
}
