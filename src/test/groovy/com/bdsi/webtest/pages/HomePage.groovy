package com.bdsi.webtest.pages

import com.bdsi.util.BDSISpockUtils
import com.bdsi.webtest.modules.Search
import geb.Page
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

class HomePage extends Page {
    static url = ""
    static at = {
        if(!(driver.getPageSource().contains("404 - Not Found")||driver.getPageSource().contains("Service Unavailable"))){
            //pageTitle.displayed
            title.contains("Search - Boeing Distribution Services")

        }

    }
    static content = {
        search { module Search }
        logo(wait: true) { $("h2.brand-boeing") }
        //pageTitle(wait: true) { $("h2.brand-boeing") }
        login(wait: true) { $("span.login-or-register") }
        username(wait: true) { $("#c-username") }
        password(wait: true) { $("#c-password") }
        loginBtn(wait: true) { $("#logInPopup") }
        logoutIcon(wait: true) { $("img.user-icon") }
        logoutLink(wait: true) { $(By.xpath("//a[@id='logOutButton']")) }
        popupLoginError(wait: true) { $(By.xpath("//p[contains(text(),'Username is incorrect, Please try again.')]")) }
        popupErrorPwd(wait: true) { $(By.xpath("//p[contains(text(),'Password is incorrect, Please try again')]")) }
        footerFirstTimeUserGuideLink(wait: true) { $(By.linkText("First Time User Guide")) }
        footerFrequentlyAskedQuestionsLink(wait: true) { $(By.linkText("Frequently Asked Questions")) }
        footerRegisterLink(wait: true) { $(By.linkText("Register")) }
        footerFeedbackLink(wait: true) { $(By.linkText("Feedback")) }
        footerContactSupportLink(wait: true) { $(By.linkText("Contact Support")) }
        footerPrivacyPolicyLink(wait: true) { $(By.linkText("Privacy Policy")) }
        footerEUDataPrivacyNoticeLink(wait: true) { $(By.linkText("EU Data Privacy Notice")) }
        footerTermsofUseLink(wait: true) { $(By.linkText("Terms of Use")) }
        footerConditionsofSaleLink(wait: true) { $(By.linkText("Conditions of Sale")) }
        activatesession(wait: true) { $("button#activateSession") }
        addPart(wait: true){$(By.xpath("//span[@class='in-lbl']"))}
        viewCartMini(required : false,wait:10){$(By.xpath("//button[contains(text(),'VIEW CART')]"))}
        loginloader(wait: 10, required : false){$("#loadingDiv")}
        inventoryNotAvailable(required: false,wait:10){$()}
        orderHistoryLink(wait: true){$(By.xpath("//a[contains(text(),'Order History')]"))}
        pleaseLoginToSeePricing(wait:true){$(By.className("login_link"))}
        loginPopUpBox(wait:true){$(By.xpath("//h2[contains(text(),'Log In')]"))}
        contactYourSalesRepAnonymous(wait:true){$(By.xpath("//a[@id='epopuplink']"))}
        pleaseLoginToContactSalesRepPopUP(wait:true){$(By.xpath("//body/div[@id='contact-your-salesrep-modal']/div[1]/div[1]/div[2]/div[1]/p[1]"))}
        faqFooterLink(wait:true){$(By.xpath("//a[contains(text(),'Frequently Asked Questions')]"))}
        searchTemplateDownload(wait:true){$(By.xpath("//a[contains(text(),'template.')]"))}
    }

    def scrollUp(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${-distance});");
        sleep(100)
    }

    def scrollDown(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${distance});");
        sleep(100)
    }

    def Login() {
        login.click()
        sleep(3000)
        username = BDSISpockUtils.getConfigProperty('uName')
        password = BDSISpockUtils.getConfigProperty('pwd')
        loginBtn.click()
        WebDriverWait wait=new WebDriverWait(driver,3000)
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("#loadingDiv")))
        VerifyLogin()
    }
    def Login(String uname) {
        login.click()
        sleep(3000)
        username = uname
        password = BDSISpockUtils.getConfigProperty('pwd')
        loginBtn.click()
        WebDriverWait wait=new WebDriverWait(driver,3000)
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("#loadingDiv")))
        VerifyLogin()
    }

    def LoginCreditCardUser() {
        login.click()
        sleep(3000)
        username = BDSISpockUtils.getConfigProperty('creditCardUname')
        password = BDSISpockUtils.getConfigProperty('pwd')
        loginBtn.click()
        WebDriverWait wait=new WebDriverWait(driver,3000)
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("#loadingDiv")))
        VerifyLogin()
    }

    def LoginASLUser() {
        login.click()
        sleep(3000)
        username = BDSISpockUtils.getConfigProperty('aslUname')
        password = BDSISpockUtils.getConfigProperty('pwd')
        loginBtn.click()
        WebDriverWait wait=new WebDriverWait(driver,3000)
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("#loadingDiv")))
        VerifyLogin()
    }

    def Login_Check(String uname, String pass) {
        login.click()
        sleep(3000)
        username = uname
        password = pass
        loginBtn.click()
        WebDriverWait wait=new WebDriverWait(driver,3000)
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("#loadingDiv")))
    }

    def VerifyLogin() {
        WebDriverWait wait=new WebDriverWait(driver,3000)
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("#loadingDiv")))
        sleep(3000)
        if (activatesession.displayed) {
            activatesession.click()
            //wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("#loadingDiv")))
            sleep(3000)
        }
        try {
            waitFor { logoutIcon.displayed }
            logoutIcon.displayed
        }
        catch (Exception ignored) {
            driver.navigate().refresh()
        }
    }

    def maximize_window(){
        driver.manage().window().maximize()
    }


    def Logout() {
        logoutIcon.click()
        logoutLink.click()
    }

}
