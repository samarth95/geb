package com.bdsi.webtest.pages

import geb.Page
import org.openqa.selenium.By

class OrderHistoryPage extends Page {

    static at = {
        title.contains("Order History - Boeing Distribution")
    }


    static content = {
        heading { $(By.xpath("//span[@id='']")).text() }
        orderhistoryPageVerification (wait:true){$(By.xpath("//h4[contains(text(),'Order History')]"))}
        userIcon(wait: true){$(By.xpath("//img[contains(@src,'/assets/rebrand/images/user-icon.svg')]"))}
        logoutLink(wait:true){$(By.xpath("//a[@id='logOutButton']"))}
    }
}
