package com.bdsi.webtest.pages

import geb.Page
import org.openqa.selenium.By

class PDPPage extends Page{

    static at = {
        sleep(3000)
        title.contains("Buy now at Boeing Distribution")
    }


    static content = {
        heading { $(By.xpath("//span[@id='']")).text() }
        partImage (wait:true){$("img#zoomit-target")}
        returnToSearchResult(wait:true){$("span.left-rt")}
        /*outOfStockPDP(wait:true){$('div').find(text:'OUT OF STOCK')}
        inStockPDP(wait:true){$('div').find(text:'IN STOCK')}*/
        /*outOfStockPDP(wait:true){find('div',text:"OUT OF STOCK")}
        inStockPDP(wait:true){find('div',text:"IN STOCK")}*/
        //outOfStockPDP(wait:true){$(By.xpath("//div[contains(text(),'OUT OF STOCK')]"))}
        outOfStockPDP(wait:true){$("div.out-of-stock")}
        inStockPDP(wait:true){$("div.in-stock")}
     /*   outOfStockPDP(wait:true){$(By.linkText("OUT OF STOCK"))}
        inStockPDP(wait:true){$(By.linkText("IN STOCK"))}*/
    }
}
