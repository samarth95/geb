package com.bdsi.webtest.pages

import geb.Page

import org.openqa.selenium.By


class CartPage extends Page{
    static at={
        assert title == "Cart - Boeing Distribution"
        if(title.contains("Common-Http-Error-Page")){
            reportInfo("Cart not accessible hence failing test case")
        }
    }
    static content={
        updateAll(wait: 10, required : false){$("button#updateLineMinimum")}
        emptyAll(wait:true){$(By.xpath("//span[contains(text(),'Empty Cart')]"))}
        searchForPart(wait:true){$(By.xpath("//a[contains(text(),'Search For Parts')]"))}
        qtyUpdate(wait: true){$(By.xpath("/html[1]/body[1]/div[1]/section[1]/div[1]/div[1]/div[4]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[9]/button[1]"))}
        splitQuantity(wait: true){$(By.xpath("i.split-text"))}
        splitQuantityBox(wait: true){By.xpath("//tbody/tr[1]/td[1]/input[1]")}
        splitQuantityUpdate(wait: true){By.xpath("//div[contains(text(),'UPDATE')]")}
        splitQuantityOk(wait: true){By.xpath("//button[@id='splitLine']")}
        qtysplit(wait: true){By.linkText("QTY SPLIT")}
        POBox{$("input#poNumber")}
        agreementCheckbox{$("input#acceptAgreeMent")}
        ultDestCheckbos{$("input#udAddressCorrect")}
        purchaseButton{$("a#advancePurchase")}
        savedAsQuote(wait:true){$(By.xpath("//a[@id='createSaveQuote']"))}
        saveQuotePopUp(wait:true){$(By.xpath("//h2[contains(text(),'Save as quote')]"))}
        savedQuoteName(wait:true){$(By.xpath("//input[@id='quote-name']"))}
        saveQuoteOkButton(wait:true){$(By.xpath("//body/div[1]/div[6]/div[1]/div[1]/div[2]/div[1]/form[1]/div[3]/div[1]/button[1]"))}

    }
}


