package com.bdsi.webtest

import com.bdsi.webtest.pages.FAQ
import com.bdsi.webtest.pages.HomePage
import geb.spock.GebReportingSpec
import org.openqa.selenium.JavascriptExecutor
import spock.lang.Title

@Title("Miscellaneous")
class Miscellaneous extends GebReportingSpec{

    /* Utility Methods Start*/

    def "setup"() {
        driver.manage().window().maximize()
    }
    void scrollUp(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${-distance});");
        sleep(100)
    }
    void scrollDown(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${distance});");
        sleep(100)
    }

    /* Test cases start here */

    def "Verify that user is able to navigate to FAQ page by clicking on the link in footer"(){
        given: "I opens E-comm website"
        to HomePage
        when:"I scroll down and click on FAQ link in footer"
        at HomePage
        scrollDown(2000)
        faqFooterLink.click()
        Thread.sleep(2000)
        then:"I should land to FAQ page"
        at FAQ
        then:"I close the E-com Website"

    }

    def "Verify that user is able to download the search template"(){
        given: "I opens E-comm website"
        to HomePage
        when:"I click on template link above search box"
        at HomePage
        searchTemplateDownload.click()
        then:"A excel file should be downloaded"
    }


}
