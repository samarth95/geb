package com.bdsi.webtest

import com.bdsi.webtest.pages.HomePage
import geb.spock.GebReportingSpec
import org.openqa.selenium.JavascriptExecutor
import spock.lang.Title
import spock.lang.Unroll



@Title("Search")
class SearchTest extends GebReportingSpec {
    def setup() {
        driver.manage().window().maximize()
    }

    void scrollUp(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${-distance});");
        sleep(100)
    }

    void scrollDown(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${distance});");
        sleep(100)
    }

    def "HardwarePart Search"() {
        given: "User opens E-comm website"
        to HomePage
        when: "Anonymous user search for Hardware Part"
        search.SearchPart("AN3-4")
        then: "User verifies search result"
        scrollDown(400)
        search.searchResult.displayed
        and: "User close the browser"
    }

    def "ChemicalPart Search"() {
        given: "User opens E-comm website"
        to HomePage
        when: "Anonymous user search for Chemical Part"
        search.SearchPart("0XY-B0ND-109DP-50ML")
        then: "User verifies search result"
        scrollDown(400)
        search.searchResult.displayed
        and: "User close the browser"
    }

    def "LightingPart Search"() {
        given: "User opens E-comm website"
        to HomePage
        when: "Anonymous user search for Lighting Part"
        search.SearchPart("1818")
        then: "User verifies search result"
        scrollDown(400)
        search.searchResult.displayed
        and: "User close the browser"
    }

    @Unroll
    def "Check Inventory for #partName"() {
        given: "User opens E-comm website"
        to HomePage
        when: "Anonymous user search for any Part"
        search.SearchPart(partName)
        sleep(2000)
        if ($("#epopuplink").displayed) {
            reportInfo("Unavailable")
        } else {
            reportInfo("Available")
        }
        then:"checking"
        where:"Search Result is:-"
        partName                           |partType
        "An3-10"                           |"Hardware"
        "1818"                             |"Lighting"
        "0XY-B0ND-109DP-50ML"              |"Chemical"
    }
    @Unroll
    def "Anonymous user Search for #partName"(){
        given:"Anonymous user opens E-comm website"
        to HomePage
        when:"Anonymous user search for #partName"
        search.SearchPart(partName)
        then:"Search result is displayed for #partName"
        scrollDown(400)
        search.searchResult.displayed
        where:"Search Result is:-"
        partName                           |partType
        "An3-10"                           |"Hardware"
        "An3-5"                            |"Hardware"
        "1818"                             |"Lighting"
        "0XY-B0ND-109DP-50ML"              |"Chemical"
    }

    def "Anonymous user login from search result"(){
        given:"Anonymous user open E-comm website"
        to HomePage
        when:"Anonymous user search for any part"
        search.SearchPart("AN3-3")
        then:"Please login link is present in search result"
        pleaseLoginToSeePricing.displayed
        when:"User clicks on login link then login box should appear"
        loginPopUpBox.displayed
        then:"USer closes the browser"

    }
}



