package com.bdsi.webtest

import com.bdsi.webtest.pages.CartPage
import com.bdsi.webtest.pages.HomePage
import geb.spock.GebReportingSpec
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.support.ui.WebDriverWait
import spock.lang.Title
import org.openqa.selenium.support.ui.Select

import java.sql.Driver

@Title("Happy Path-Search-Cart-OrderConfirmation")
class EndToEnd extends GebReportingSpec{



    //-------------- Utility Methods ----------------------//
    def "setup"(){
        driver.manage().window().maximize()
    }

    void scrollUp(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${-distance});");
        sleep(100)
    }
    void scrollDown(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${distance});");
        sleep(100)
    }
    def add_part(String partName){
        HomePage.search.SearchPart(partName)
        addPart.click()
    }
    boolean checkInventory(){
        if(driver.getPageSource().contains("This part number is currently not available online")){
            return false;
        }
        else{
            return true;
        }

    }
    WebDriverWait wait = new WebDriverWait(driver,60)

    //----------------Test Case Start-----------------------------//
    def "I make purchase for a part form BDSI website"() {
        given: "I open E-comm Website"
        to HomePage
        maximize_window()
        when: "I Login"
        Login("sales.001953")
        then:"I check if there is any part present in cart"
        if(viewCartMini.displayed){
            viewCartMini.click()
            at CartPage
            if(updateAll.displayed){
                updateAll.click()
            }
            sleep(3000)
            emptyAll.click()
            searchForPart.click()
            at HomePage
        }
        sleep(2000)
        then:"I Search for the part and add to cart"
        search.SearchPart("AN3-3",100)
        scrollDown(500)
        sleep(3000)
        addPart.click()
        sleep(2000)
        def Uxpath="//tbody/tr[@id='AN3-3']/td[1]/div[1]"
        driver.findElement(By.xpath(Uxpath)).isDisplayed()
        sleep(5000)
        when:"I navigate to cart"
        viewCartMini.click()
        sleep(2000)
        at CartPage
        sleep(2000)
        then:"I enter shipping details and PO number"

        /*
            this can be un commented for 001953 in stg1 as it has free shipping option
         */
        /*driver.findElement(By.xpath("//tbody/tr[1]/td[14]/span[1]/button[1]")).click()
        sleep(2000)
        driver.findElement(By.xpath("//body/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/select[1]")).click()
        Select carrierdrp = new Select(driver.findElement(By.xpath("//body/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/select[1]")))
        carrierdrp.selectByValue("Free_Delivery")
        driver.findElement(By.xpath("//button[@id='saveAdvanceShippingDetails']")).click()
        */
        sleep(2000)
        POBox.value("AutoamtionTest")
        sleep(2000)
        scrollDown(500)
        driver.findElement(By.xpath("//body/div[1]/section[1]/div[1]/div[1]/div[4]/div[1]/div[5]/div[2]/label[1]/span[1]")).click()
        sleep(2000)
        driver.findElement(By.xpath("//span[contains(text(),'My Ultimate Destination is correct.')]")).click()
        when:"I click on purchase button"
        purchaseButton.click()
        sleep(3000)

        /*
            When Cart needs attention then it will work
         */
        if(driver.findElement(By.xpath("//*[contains(text(),'" + "CART NEEDS ATTENTION" + "')]")).isDisplayed()){
            driver.findElement(By.xpath("//button[contains(., 'OK')]")).click()
            driver.findElement(By.xpath("//tbody/tr[1]/td[14]/span[1]/button[1]")).click()
            sleep(2000)
            /*
                This is to Edit Shipping Method
                carrierdrp - to select carrier
                speedDrp - to select speed
                accDrp - to select accounts
             */
            driver.findElement(By.xpath("//body/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/select[1]")).click()
            Select carrierdrp = new Select(driver.findElement(By.xpath("//body/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[1]/select[1]")))
            carrierdrp.selectByValue("FedEx")
            driver.findElement(By.xpath("//body/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[2]/select[1]")).click()
            Select speedDrp = new Select(driver.findElement(By.xpath("//body/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[2]/select[1]")))
            speedDrp.selectByValue("100001~155~105")
            driver.findElement(By.xpath("//body/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[3]/select[1]")).click()
            Select accDrp = new Select(driver.findElement(By.xpath("//body/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[3]/select[1]")))
            accDrp.selectByValue("Add shipping cost to invoice")
            driver.findElement(By.xpath("//button[@id='saveAdvanceShippingDetails']")).click()
            sleep(2000)
            purchaseButton.click()
        }
        sleep(2000)
        then:"I should land on order confirmation page"
        driver.getTitle().equalsIgnoreCase("Order Confirmation - Boeing Distribution")
        sleep(2000)
        when:"I navigate back to HomePage"
        to HomePage
        then:"I Logout"
        Logout()
    }

}
