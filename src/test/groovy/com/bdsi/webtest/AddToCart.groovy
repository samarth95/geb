/*
This test case methods are to add the parts to the cart
 */
package com.bdsi.webtest

import com.bdsi.webtest.pages.CartPage
import com.bdsi.webtest.pages.HomePage
import com.bdsi.webtest.pages.SavedQuotesPage
import geb.spock.GebReportingSpec
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.StaleElementReferenceException
import spock.lang.Title
import spock.lang.Unroll

import java.util.concurrent.TimeUnit

@Title("Add To Cart")
class AddToCart extends GebReportingSpec {
    def "setup"() {
        driver.manage().window().maximize()
    }
    int yearNow ;
    int monthNow ;
    int dayNow ;
    void scrollUp(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${-distance});");
        sleep(100)
    }
    void scrollDown(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${distance});");
        sleep(100)
    }
    def add_part(String partName){
        HomePage.search.SearchPart(partName)
        addPart.click()
    }

    boolean checkInventory(){
        if(driver.getPageSource().contains("This part number is currently not available online")|| driver.getPageSource().contains(" This part number was not recognized.")||driver.getPageSource().contains("This part number is out of stock in your warehouse location. Please")){
           return false;
        }
        else{
            return true;
        }
    }

    int today_date(){
        Date now = new Date();
        Calendar calendar = Calendar.getInstance();

         yearNow = calendar.get(Calendar.YEAR)
         monthNow = calendar.get(Calendar.MONTH)
         dayNow = calendar.get(Calendar.DAY_OF_MONTH)
        return dayNow
    }

    def empty_cart() {
        viewCartMini.click()
        at CartPage
        if (updateAll.displayed) {
            updateAll.click()
            sleep(1000)
        }
        emptyAll.click()
        sleep(2000)
        searchForPart.click()
        at HomePage
    }

    def boolean isElementPresentInDom(By by) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        boolean isElementPresent = (driver.findElements(by).size() != 0);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); // I have a driverTimeOut variable, I set the default timeout to zero in the beginning of this method to make it fast, then I set it to my driverTimeOut again. I recommend you to do the same :)
        return isElementPresent;
    }

    @Unroll
    def "I try to add #partName to Cart"() {
        given: "I opens E-comm Website"
        to HomePage
        when: "I Log In"
        Login()
        sleep(2000)
        boolean Minicart = false
        Minicart =  viewCartMini.isDisplayed()
        if(Minicart){
            empty_cart()
        }
        then:"I search for part"
        search.SearchPart(partName)
        if(checkInventory()){
            addPart.click()
            def xpath="//tbody/tr[@id='"+partName+"']/td[1]/div[1]"
            driver.findElement(By.xpath(xpath)).isDisplayed()
            def xpath2="//tbody/tr[@id='"+partName+"']/td[1]/span[1]/img[1]"
            driver.findElement(By.xpath(xpath2)).click()
            sleep(2000)
            Logout()
        }
        else{
            report("Invenotry is not available for part $partName")
            Logout()
        }
        where:"Part are"
        partName                           |partType
        "AN3-10"                           |"Hardware"
        "AN3-5"                            |"Hardware"
        "0XY-B0ND-109DP-50ML"              |"Chemical"
    }

    def "Create Saved Quote"(){
        String[] parts=["AN2-3,100","AN3-10,100","0XY-B0ND-109DP-50ML,100"]
        int counter=0;
        given: "I open E-comm Website"
        to HomePage
        when: "I Log In"
        Login()
        sleep(2000)
        boolean Minicart = false
        Minicart =  viewCartMini.isDisplayed()
        if(Minicart){
            empty_cart()
        }
        then:"I search for part AN3-3"
        for(String part in parts){
            search.SearchPart(part)
            sleep(2000)
            /*if(isElementPresentInDom(By.xpath("//tbody/tr[1]/td[11]/img[1]"))){
                waitFor(10) { $(By.xpath("//tbody/tr[1]/td[11]/img[1]")).displayed }
            }*/
            boolean searchResult = checkInventory()
            if(searchResult){
                break
            }
            counter++
        }
        reportInfo(counter)
        if((!counter <= parts.size())){
            report("Inventory is not found for any parts given")
            reportFailure()
        }
        when:"Add part to the cart"
        try{
            addPart.click()
            sleep(2000)
        }
        catch (StaleElementReferenceException e){
            driver.navigate().refresh()
            sleep(3000)
            addPart.click()
        }
        then:"I navigate to the cart"
        viewCartMini.click()
        at CartPage
        if(updateAll.displayed){
            updateAll.click()
        }
        then:"I create quote for the part present in the cart"
        savedAsQuote.click()
        sleep(2000)
        if(saveQuotePopUp.displayed){
            savedQuoteName.value("")
            savedQuoteName.value("Automation quote - "+today_date())
            saveQuoteOkButton.click()
        }
        when:"Quote is created I should be navigated to Saved Quote page"
        at SavedQuotesPage
        sleep(5000)
        createdSavedQuoteName.displayed
        then:"I navigate back to Homepage"
        to HomePage
        and:"I Log Out"
        Logout()

    }
}
