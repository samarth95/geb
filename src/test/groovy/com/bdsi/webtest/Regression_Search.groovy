package com.bdsi.webtest


import com.bdsi.webtest.pages.HomePage
import geb.spock.GebReportingSpec
import org.openqa.selenium.JavascriptExecutor
import spock.lang.Title
import spock.lang.Unroll


@Title("Regression for Search")
class Regression_Search extends GebReportingSpec {

    /* Utility Methods Start*/

    def "setup"() {
        driver.manage().window().maximize()
    }
    void scrollUp(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${-distance});");
        sleep(100)
    }
    void scrollDown(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${distance});");
        sleep(100)
    }
    def add_part(String partName){
        HomePage.search.SearchPart(partName)
        addPart.click()
    }

    /* Utility Methods End */


    /* ------------Test Cases Start -------------------*/
    @Unroll
    def "Verify whether the availability and pricing is shown for a part which has only prime from the partial match"() {
        given: "User opens E-comm website"
        to HomePage
        when: "user log in"
        Login()
        then:"User search for the KLINGERFLON having only prime parts in partial match"
        search.SearchPart(partName)
        when:"User veriffies the search result"
        sleep(4000)
        search.partialSearchResult.displayed
        then:"User logs out and close the browser"
        Logout()
        where:"Search Result is:-"
        partName                           |partType
        "KLINGERFLON"                      |"Hardware"
        "1818"                             |"Lighting"
        "0XY-B0ND-109DP-50ML"              |"Chemical"

    }



    /*------- Test Cases End -------------*/

}


