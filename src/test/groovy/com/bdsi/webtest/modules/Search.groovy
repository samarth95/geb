package com.bdsi.webtest.modules

import geb.Module
import org.openqa.selenium.By

class Search extends Module {
    static content={
        searchBox(wait: true) { $("#searchInput") }
        searchBtn(wait: true) { $("button#boxSearch") }
        searchResult(wait: true) { $("span.left-head") }
        partialSearchResult(wait:true){$("div.search-err-msg")}
        //partNumberSearchClass(wait:true){$("a.gray-color historyAPI")}
        //partNumberSearchClass (wait:true){find(By.partialLinkText("RTV"))}
        partNumberSearchClass (wait:true){$(find(By.partialLinkText("RTV")))}
        errorMsg(wait: true){$(By.xpath("//tbody/tr[1]/td[3]"))}
    }

    def SearchPart(String partName) {
        searchBox = partName
        searchBtn.click()
    }
    def SearchPart(String partName,int qty) {
        searchBox = partName+","+qty
        searchBtn.click()
    }

    def String Inventory() {
        String msg = null
        try{
            if (errorMsg.displayed) {
                msg = errorMsg.getAttribute("value")
                return msg
            } else {
                msg = "Inventory available"
                return msg
            }
        }
        catch (Exception e){
            e.fillInStackTrace()
        }

    }

    def boolean partialCheck(String partName){
        if(partialSearchResult.getAttribute("value").equalsIgnoreCase("We did not find a match for "+partName+" Please select the appropriate part number below.")){
            return true;
        }
        else{
            return false;
        }
    }
}
