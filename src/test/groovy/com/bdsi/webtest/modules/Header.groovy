package com.bdsi.webtest.modules

import geb.Module

class Header extends Module{
        static content = {
            logo(wait: true) { $("h2.brand-boeing") }
            pageTitle(wait: true) { $("h2.brand-boeing") }
            login(wait: true) { $("span.login-or-register") }

        }
}
