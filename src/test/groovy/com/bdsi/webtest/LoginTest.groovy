package com.bdsi.webtest

import com.bdsi.util.BDSISpockUtils
import com.bdsi.webtest.pages.HomePage
import geb.driver.CachingDriverFactory
import geb.spock.GebReportingSpec
import org.openqa.selenium.By
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import spock.lang.Stepwise
import spock.lang.Title

@Title("Login")
class LoginTest extends GebReportingSpec {
    def setupSpec() {
    }

    def setup() {
            driver.manage().window().maximize()
    }

    def "Login with Valid User"() {
        given: "User open's Website and Login"
        to HomePage
        when: "User log's in with correct credentials"
        Login()
        then: "Verify that Login is successful"
        VerifyLogin()
        then:"User Log's out"
        Logout()
    }
    def "Login with invalid credentials"(){
        given:"User open's Website and try to Login with wrong credentials"
        to HomePage
        when: "User try to log in with incorrect credentials"
        Login_Check('sales','pass')
        then: "Verify that error Login message is displayed"
        sleep(2000)
        popupLoginError.displayed
        and:"User close the browser"
    }
    def "Login with invalid Username"(){
        given:"User open's Website and try to Login with wrong credentials"
        to HomePage
        when: "User try to log in with incorrect Username"
        Login_Check("sales","password")
        then: "Verify that error Login message is displayed"
        sleep(2000)
        popupLoginError.displayed
        and:"User close the browser"
    }
    def "Login with invalid Password"(){
        given:"User open's Website and try to Login with wrong credentials"
        to HomePage
        when: "User try to log in with incorrect Password"
        Login_Check("sales.001020","pass")
        then: "Verify that wrong-password error Login message is displayed"
        sleep(2000)
        popupErrorPwd.displayed
        and:"User close the browser"
    }
    def "Activate-Session functionality -I"(){
        given: "User open's Website and Login"
        to HomePage
        when: "User log's in"
        Login()
        then: "Verify that Login is successful"
        VerifyLogin()
    }
    def "Activate-Session functionality-II"(){
        given:"User is on E-comm Website"
        to HomePage
        when:"User log's in again with the same credentials"
        login.click()
        sleep(1000)
        username = "sales.001020"
        password = "password"
        loginBtn.click()
        sleep(2000)
        WebDriverWait wait=new WebDriverWait(driver,5000)
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("#loadingDiv")))
        then:"User must see Active-Session Pop-up"
        sleep(2000)
        activatesession.displayed
        then:"User activates the current session"
        activatesession.click()
        sleep(3000)
        then:"Than verify that session is activated and user is logged in"
        VerifyLogin()
        then:"User Log's out"
        Logout()
    }

}
