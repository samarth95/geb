package com.bdsi.webtest

import com.bdsi.webtest.pages.HomePage
import geb.spock.GebReportingSpec
import org.openqa.selenium.JavascriptExecutor
import spock.lang.Title

@Title("POP_UP Verifications")
class Pop_Up_Verification extends GebReportingSpec{

    /* Utility Methods Start*/

    def "setup"() {
        driver.manage().window().maximize()
    }
    void scrollUp(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${-distance});");
        sleep(100)
    }
    void scrollDown(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${distance});");
        sleep(100)
    }

    /* Test cases start here */

    def "Verify that Anonymous user cannot contact sales rep"(){
        given: "I opens E-comm website"
        to HomePage
        when:"I search for a part which do not have inventory"
        search.SearchPart("MOMENTIVE")
        scrollDown(500)
        then:"I verify that contact you sales rep link is present"
        Thread.sleep(2000)
        at HomePage
        contactYourSalesRepAnonymous.click()
        when:"I click on contact sales rep link then i should get a pop-up saying please login"
        pleaseLoginToContactSalesRepPopUP.displayed
        then:"I close the E-com Website"

    }
}
