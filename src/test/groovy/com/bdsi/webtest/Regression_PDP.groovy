package com.bdsi.webtest

import com.bdsi.webtest.pages.HomePage
import com.bdsi.webtest.pages.PDPPage
import geb.spock.GebReportingSpec
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.StaleElementReferenceException
import spock.lang.Title
import spock.lang.Unroll

@Title("Regression for PDP")
class Regression_PDP extends GebReportingSpec{

    /* Utility Methods Start*/

    def "setup"() {
        driver.manage().window().maximize()
    }
    void scrollUp(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${-distance});");
        sleep(100)
    }
    void scrollDown(int distance) {
        ((JavascriptExecutor) driver).executeScript("scroll( 0, ${distance});");
        sleep(100)
    }
    def add_part(String partName){
        HomePage.search.SearchPart(partName)
        addPart.click()
    }
    boolean checkInventory(){
        if(driver.getPageSource().contains("This part number is currently not available online")|| driver.getPageSource().contains(" This part number was not recognized.")||driver.getPageSource().contains("This part number is out of stock in your warehouse location. Please")){
            return false;
        }
        else{
            return true;
        }
    }
    /* Utility Methods End */


    @Unroll
    def "Verify whether the images (squeeze tubes) are displayed only for the #partName prime part numbers"(){
        given: "I opens E-comm website"
        to HomePage
        when:"I log in"
        Login()
        and:"I search for the part"
        search.SearchPart(partName)
        then:"I verify that search result is displayed properly"
        scrollDown(700)
        int attempts=0
        while(attempts<2){
            try{
                search.partNumberSearchClass.displayed
                break
            }
            catch(StaleElementReferenceException e){

            }
            attempts++
        }
        when:"I land on PDP page of #partName part"
        sleep(2000)
        attempts=0
        while(attempts<2){
            try{
                search.partNumberSearchClass.click()
                break
            }
            catch(StaleElementReferenceException e){

            }
            attempts++
        }
        at PDPPage
        then:"I verify that correct image is displayed"
        sleep(5000)
        partImage.displayed
        when:"I navigate back to the home page"
        to HomePage
        then:"I log out"
        Logout()
        where:"Search Result is:-"
        partName                            |partType
        "RTV106-310ML"                      |"Hardware"
        "RTV133-310ML"                      |"Hardware"
        "RTV160-310ML"                      |"Hardware"
        "RTV162-310ML"                      |"Hardware"
        "RTV167GREY-300ML"                  |"Hardware"
        "RTV-1473-310ML"                    |"Hardware"
        "RTV-6708-335GM"                    |"Hardware"
        "RTV-102-WHT-82-8ML"                |"Hardware"
        "RTV103-82ML"                       |"Hardware"
        "RTV106-82ML"                       |"Hardware"
        "RTV108-82ML"                       |"Hardware"
        "RTV112-87GM"                       |"Hardware"
        "RTV-118-CLEAR-82-8ML"              |"Hardware"
        "RTV-157-GREY-82-8ML"               |"Hardware"
        "RTV-159-RED-82-8ML"                |"Hardware"
        "RTV162-82ML"                       |"Hardware"
        "RTV167GREY-90GM"                   |"Hardware"
        "RTV-6708-90GM"                     |"Hardware"
    }

    def "Verify whether the image is shown for the prime part other than the below"(){
        given: "I open E-comm website"
        to HomePage
        when:"I log in"
        Login()
        then:"I search for the part"
        search.SearchPart("RTV157-150ML")
        sleep(2000)
        then:"I verify that search result is displayed properly"
        scrollDown(700)
        int attempts=0
        while(attempts<2){
            try{
                search.partNumberSearchClass.displayed
                break
            }
            catch(StaleElementReferenceException e){

            }
            attempts++
        }
        when:"I land on PDP page of  RTV157-150ML"
        search.partNumberSearchClass.click()
        sleep(2000)
        at PDPPage
        then:"I verify that correct image is displayed"
        sleep(2000)
        boolean image_not_visible=null
            try{
                driver.findElement(By.cssSelector("img#zoomit-target")).isEnabled()
                }
        catch (NoSuchElementException e){
            image_not_visible=false
        }

        assert image_not_visible == false
        then:"O navigate back to the home page and log out"
        returnToSearchResult.click()
        at HomePage
        Logout()
    }

    def "Verify whether the label color is shown as IN STOCK- in green color OUT OF STOCK- in red color"() {
        given: "I open E-comm website"
        to HomePage
        when: "I log in"
        Login()
        then: "I search for the part"
        search.SearchPart("RTV157-150ML")
        when: "I check if part is in stock or not"
        scrollDown(700)
        sleep(3000)
        boolean stock=null
        if(checkInventory()){
            stock=true
            reportInfo("Part is in stock and color should be GREEN")

        }
        else{
            stock=false
            reportInfo("Part is out of stock and color should be RED")
        }

        then: "I land on PDP page of  RTV157-150ML"
        //search.partNumberSearchClass.displayed
        search.partNumberSearchClass.click()
        sleep(3000)
        at PDPPage
        then: "I verify that stock status is visible on PDP page and text colour is correct"
        if(stock){
            assert "rgba(0, 128, 0, 1)" == inStockPDP.css("color")
        }
        else if(stock.equals(false)){
            assert "rgba(183, 0, 0, 1)" == outOfStockPDP.css("color")
        }

    }

}
