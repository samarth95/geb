package com.bdsi.webtest

import com.bdsi.webtest.pages.HomePage
import com.bdsi.webtest.pages.ProfilePage
import com.google.gson.annotations.Until
import geb.spock.GebReportingSpec
import org.openqa.selenium.By
import spock.lang.Title

@Title(value = "My Account Page")
class MyAccount extends GebReportingSpec{

    def "setup"(){
        driver.manage().window().maximize()
    }

    def "Access My Account Page"(){
        given:"I opens E-comm website"
        to HomePage
        sleep(2000)
        when:"I login "
        Login()
        then:"I click on user icon"
        sleep(2000)
        driver.findElement(By.xpath("//img[contains(@src,'/assets/rebrand/images/user-icon.svg')]")).click()
        sleep(2000)
        when:"User dropdown opens"
        sleep(2000)
        then:"I click on Profile"
        driver.findElement(By.xpath("//a[contains(text(),'Profile')]")).click()
        sleep(2000)
        when:"I am at My Account Page"
        at ProfilePage
        sleep(2000)
        then:"I verify My account details"
        driver.findElement(By.xpath("//p[contains(text(),'Company Name')]")).isDisplayed()
        then:"I logout"
        driver.findElement(By.xpath("//img[contains(@src,'/assets/rebrand/images/user-icon.svg')]")).click()
        sleep(1000)
        driver.findElement(By.xpath("//a[@id='logOutButton']"))

    }
}
