package com.bdsi.webtest

import com.bdsi.webtest.pages.HomePage
import com.bdsi.webtest.pages.OrderHistoryPage
import geb.spock.GebReportingSpec
import org.openqa.selenium.By
import spock.lang.Title


@Title(value = "Order history Page")
class OrderHistory extends GebReportingSpec{

    def "setup"(){
        driver.manage().window().maximize()
    }

    def "Verify Order history page"() {
        given: "I opens E-comm website"
        to HomePage
        sleep(2000)
        when: "I login"
        Login()
        sleep(2000)
        then:"I click on user icon"
        logoutIcon.click()
        when:"I click on Order History"
        orderHistoryLink.click()
        sleep(100000)
        then:"I land on Order history page"
        at OrderHistoryPage
        when:"I verify Order history page"
        orderhistoryPageVerification.displayed
        sleep(5000)
        then:"I go to HomePage and Logout"
        to HomePage
        logoutIcon.click()
        sleep(2000)
        logoutLink.click()
    }
}

