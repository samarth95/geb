package com.bdsi.webtest

import com.bdsi.webtest.pages.HomePage
import geb.spock.GebReportingSpec
import spock.lang.Title

@Title(value = "Saved Quotes Page")
class Quotes extends GebReportingSpec{

    def "setup"(){
        driver.manage().window().maximize()
    }

    def "Verify Saved Quotes page"(){
        given:"I opens E-comm website"
        to HomePage
        sleep(2000)
        when:"I login"
        sleep(2000)
        then:"I click on user icon"
        when:"Drop down is openned"
        sleep(2000)
        then:"I click on Saved Quotes link"
        when:"I land on Saved Quotes page"
        sleep(2000)
        then:"I verify page links"
    }


}
