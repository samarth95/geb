package com.bdsi.util

import geb.ConfigurationLoader

import java.text.MessageFormat

class BDSISpockUtils {
    static final GEB_CONF = new ConfigurationLoader().getConf()
    static final String REPORT_DIR = GEB_CONF.reportsDir

    static def getConfigProperty(String propertyName){
        FileReader  read = new FileReader("src/test/resources/${System.properties["geb.build.baseUrl"]}.properties")
        Properties p = new Properties()
        p.load(read)
        return p.getProperty(propertyName)
    }

    static def getReportPortalUrl() {
        String portalUrl = getConfigProperty('reportPortalUrl')
        String env = System.properties['geb.build.baseUrl']
        String reportsFolder = System.properties['geb.reportsFolder']
        return MessageFormat.format(portalUrl, env, reportsFolder)
    }

}
